// Fill out your copyright notice in the Description page of Project Settings.

#include "AfterImage.h"
#include "SampleActorClass.h"


// Sets default values
ASampleActorClass::ASampleActorClass()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASampleActorClass::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASampleActorClass::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

